package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.ProjectRepository;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    @Autowired
    public ProjectRepository projectRepository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @Override
    @Transactional
    public void addAll(@NotNull final List<Project> projects) {
        for (Project project : projects) {
            projectRepository.save(project);
        }
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        User user = userRepository.findById(userId).orElse(null);
        project.setUser(user);
        project.setName(name);
        projectRepository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        User user = userRepository.findById(userId).orElse(null);
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Project project = findById(userId, id);
        project.setStatus(Status.IN_PROGRESS);
    }

    @Override
    @Transactional
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        Project project = findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = findByName(userId, name);
        project.setStatus(Status.IN_PROGRESS);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Project project = findById(userId, id);
        project.setStatus(Status.COMPLETED);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        Project project = findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = findByName(userId, name);
        project.setStatus(Status.COMPLETED);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        Project project = findById(userId, id);
        project.setStatus(status);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        Project project = findByIndex(userId, index);
        project.setStatus(status);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        Project project = findByName(userId, name);
        project.setStatus(status);
        projectRepository.save(project);
    }

}
