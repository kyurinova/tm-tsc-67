package ru.tsc.kyurinova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    void deleteById(@NotNull String id);

    void deleteByLogin(@NotNull String login);

    Optional<UserDTO> findById(@NotNull String id);

    long count();

}
