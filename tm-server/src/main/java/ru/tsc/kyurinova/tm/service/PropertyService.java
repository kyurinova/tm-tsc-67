package ru.tsc.kyurinova.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['session.secret']}")
    private String sessionSecret;

    @Value("#{environment['session.iteration']}")
    public Integer sessionIteration;

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['developer.name']}")
    private String developerName;

    @Value("#{environment['developer.email']}")
    private String developerEmail;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['jdbc.user']}")
    private String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    private String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    private String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    private String jdbcDriver;

    @Value("#{environment['jdbc.sql_dialect']}")
    private String jdbcSqlDialect;

    @Value("#{environment['jdbc.hbm2ddl_auto']}")
    private String jdbcNbm2ddlAuto;

    @Value("#{environment['jdbc.show_sql']}")
    private String jdbcShowSql;

    @Value("#{environment['database.format_sql']}")
    private String formatSql;

    @Value("#{environment['database.second_lvl_cache']}")
    private String secondLevelCache;

    @Value("#{environment['database.factory_class']}")
    private String cacheRegionFactory;

    @Value("#{environment['database.use_query_cache']}")
    private String useQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    private String useMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String cacheRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    private String cacheProviderConfig;

}
