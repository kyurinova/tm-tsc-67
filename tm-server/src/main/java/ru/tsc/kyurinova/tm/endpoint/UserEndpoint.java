package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kyurinova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<UserDTO> findAllUser(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session);
        return userService.findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    UserDTO findByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return userService.findByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session);
        return userService.existsById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session);
        return userService.existsByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByEmailUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session);
        return userService.findByEmail(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isLoginExistsUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session);
        userService.isLoginExists(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isEmailExists(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session);
        userService.isEmailExists(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session);
        return userService.findByLogin(login);
    }

}
