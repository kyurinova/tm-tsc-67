package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "row_id")
    protected String id = UUID.randomUUID().toString();

    @NotNull
    public ProjectDTO(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public ProjectDTO(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column(name = "descr")
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_dt")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    private Date finishDate;

    @Nullable
    @Column
    private Date created = new Date();

}