package ru.tsc.kyurinova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kyurinova.tm.api.service.model.IProjectService;
import ru.tsc.kyurinova.tm.api.service.model.ITaskService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Task;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() throws Exception {
        @NotNull final Task task = new Task("New Task" + System.currentTimeMillis());
        taskService.add(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) throws Exception {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) throws Exception {
        if (task.getProject() == null)
            task.setProject(null);
        else
            task.setProject(projectService.findOneById(task.getProject().getId()));
        taskService.update(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) throws Exception {
        @NotNull final Task task = taskService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

}
