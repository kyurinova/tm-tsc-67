package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

}
