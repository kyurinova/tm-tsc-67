package ru.tsc.kyurinova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "row_id")
    @Id
    protected String id = UUID.randomUUID().toString();

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column(name = "descr")
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Nullable
    @Column(name = "start_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @NotNull
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

}
