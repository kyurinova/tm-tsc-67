package ru.tsc.kyurinova.tm.service.DTO;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyFieldException;
import ru.tsc.kyurinova.tm.exeption.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exeption.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.repository.DTO.TaskDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTOService implements ITaskDTOService {

    @Nullable
    @Autowired
    private TaskDTORepository repository;

    @Override
    @Transactional
    public @NotNull TaskDTO add(@NotNull TaskDTO model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public @Nullable List<TaskDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<TaskDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable TaskDTO model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void update(@Nullable TaskDTO model) throws Exception {
        if (model == null) throw new TaskNotFoundException();
        repository.save(model);
    }

    @Override
    @Transactional
    public void changeTaskDTOStatusById(@Nullable String id, @Nullable Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        if (status == null) throw new EmptyFieldException("Status");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public @NotNull TaskDTO create(@Nullable String name) throws Exception {
        if (name == null) throw new EmptyFieldException("Name");
        @Nullable final TaskDTO task = new TaskDTO(name);
        return repository.save(task);
    }

    @Override
    @Transactional
    public @NotNull TaskDTO create(@Nullable String name, @Nullable String description) throws Exception {
        if (name == null) throw new EmptyFieldException("Name");
        if (description == null) throw new EmptyFieldException("Description");
        @Nullable final TaskDTO task = new TaskDTO(name, description);
        return repository.save(task);
    }

    @Override
    public @Nullable List<TaskDTO> findAllByProjectId(@Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        if (name == null) throw new EmptyFieldException("Name");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable String id, @Nullable String projectId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("ProjectId");
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }
}
