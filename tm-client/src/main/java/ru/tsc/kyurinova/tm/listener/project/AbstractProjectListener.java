package ru.tsc.kyurinova.tm.listener.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.listener.AbstractListener;

import java.util.Optional;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    protected void showProject(@Nullable ProjectDTO project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
